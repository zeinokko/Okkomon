﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OkkomonSpawner : MonoBehaviour
{
    public bool spawnerActive;

    public float minWait;
    public float maxWait;

    private bool isSpawning;
    public AudioSource sfx;
    public AudioClip sfxClip;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    void Awake()
    {
        isSpawning = false;
        sfx = gameObject.GetComponent<AudioSource>();
    }


    // Update is called once per frame
    void Update()
    {
        if (!isSpawning && spawnerActive)
        {
            float timer = Random.Range(minWait, maxWait);
            Invoke("SpawnObject", timer);
            isSpawning = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            spawnerActive = true;
        }
    }

    void SpawnObject()
    {
        if (spawnerActive)
        {
            sfx.PlayOneShot(sfxClip);
            Debug.Log("COMBAT START!");
            isSpawning = false;
            StartCoroutine(WaitThenCombat());
        }
    }

    IEnumerator WaitThenCombat()
    {
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(1);

    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Player")
        {
            spawnerActive = false;
        }
    }



}
